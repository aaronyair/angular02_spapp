import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HeroesService, Heroe } from '../../services/heroes.service';

@Component({
  selector: 'app-heroe-busqueda',
  templateUrl: './heroe-busqueda.component.html',
  styles: []
})
export class HeroeBusquedaComponent implements OnInit {
  heroe: Heroe[] = [];

  constructor(private activatedRoute: ActivatedRoute, private _heroesService: HeroesService) {

  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.heroe = this._heroesService.buscarHeroe(params.nombreHeroe);
      console.log(this.heroe);
    });
  }

}
